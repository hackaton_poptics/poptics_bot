## bot for register participants on hackaton

### Install
1. Clone repo
2. Create .env file in repo root and populate with the following environments:  
If your running long-polling:
```  
POLLER_TYPE=longpolling  
MONGODB_URI=INSERT_MONGO_URI_HERE
BOT_TOKEN=TOKEN_HERE
```  
If you running webhook:  
```  
POLLER_TYPE=webhook  
MONGODB_URI=INSERT_MONGO_URI_HERE  
BOT_TOKEN=TOKEN_HERE
APP_PORT=1234
PUBLIC_URL=https://ip/bot_token
CERT_PATH=/home/app/cert.pem
```
if you are running wehook you also need to setup NGINX. Example config in `deploy/nginx/bot.conf`  

3; run mongodb:  
https://docs.mongodb.com/manual/administration/install-on-linux/  
or you can use docker-compose file from repo
 
4; Run bot:
`make build && make run` 