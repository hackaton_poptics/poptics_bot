package main

import (
	_ "github.com/joho/godotenv/autoload"
	"go.mongodb.org/mongo-driver/bson"
	tb "gopkg.in/tucnak/telebot.v2"
	"log"
	"os"
	"poptics_bot/pkg/database"
	"poptics_bot/pkg/handlers"
	"poptics_bot/pkg/helpers"
	"regexp"
	"strings"
)

func main() {
	var (
		//pollerType        = os.Getenv("POLLER_TYPE")
		mongoDbConnection = os.Getenv("MONGODB_URI")
	)
	pollerType := os.Getenv("POLLER_TYPE")
	mh := database.NewHandler(mongoDbConnection)
	bot, err := helpers.GetBot(pollerType)
	if err != nil {
		log.Fatal(err)
		return
	}
	if err != nil {
		log.Fatal(err)
		return
	}

	bot.Handle(tb.OnText, func(msg *tb.Message) {
		switch msg.Text {
		case "/start":
			handlers.Start(msg, bot, mh)
		default:
			user, err := mh.GetUser(bson.M{"telegram_id": msg.Sender.ID})
			if err != nil {
				log.Println(err)
			}
			switch user.State {
			case "writing_description_participant":
				handlers.SaveParticipantReq(msg, bot, mh)
			case "writing_description_lead":
				handlers.SaveLeadReq(msg, bot, mh)
			default:
				log.Println("some free space here")
			}
		}
	})
	bot.Handle(tb.OnCallback, func(cb *tb.Callback) {
		switch cb.Data {
		case "lf_team":
			handlers.RegisterParticipant(cb, bot, mh)
		case "lf_participant":
			handlers.RegisterLead(cb, bot, mh)
		case "list_requests":
			handlers.ListRequestsMenu(cb, bot, mh)
		case "list_requests_participant":
			handlers.GetRequestsFromParticipants(cb, bot, mh, false)
		case "list_requests_leads":
			handlers.GetRequestsFromLeads(cb, bot, mh, false)
		case "apply_request":
			handlers.RequestMenu(cb, bot, mh)
		case "home":
			handlers.StartInline(cb, bot, mh)
		case "back_inline":
			handlers.BackInline(cb, bot, mh)
		default:
			// TODO: match here
			if strings.Contains(cb.Data, "admin") {
				log.Println(cb.Data)
				// admin_lead_523792555
				match, err := regexp.MatchString(`^\fadmin_(participant|lead)_(\d+)$`, cb.Data)
				if err != nil {
					log.Println(err)
				}
				log.Println("match:", match)
				if match == true {
					handlers.WatchRequest(cb, bot, mh)
				}
			} else if strings.Contains(cb.Data, "decline") {
				handlers.DeclineRequest(cb, bot, mh)
			} else if strings.Contains(cb.Data, "detail_req_participant_") {
				handlers.DetailRequestsFromParticipant(cb, bot, mh)
			} else if strings.Contains(cb.Data, "detail_req_lead_") {
				handlers.DetailRequestsFromLead(cb, bot, mh)
				//
			} else if strings.Contains(cb.Data, "participant_next_page") {
				handlers.GetRequestsFromParticipants(cb, bot, mh, true)
			} else if strings.Contains(cb.Data, "participant_prev_page") {
				handlers.GetRequestsFromParticipants(cb, bot, mh, true)
				//
			} else if strings.Contains(cb.Data, "lead_next_page") {
				handlers.GetRequestsFromLeads(cb, bot, mh, true)
			} else if strings.Contains(cb.Data, "lead_prev_page") {
				handlers.GetRequestsFromLeads(cb, bot, mh, true)
			} else {
				match, err := regexp.MatchString(`^\faccept_req_lead_(\d+)`, cb.Data)
				if err != nil {
					log.Println(err)
				}
				if match == true {
					handlers.AcceptReq(cb, bot, mh)
					return
				}
				match, err = regexp.MatchString(`\faccept_req_participant_(\d+)`, cb.Data)
				if err != nil {
					log.Println(err)
				}
				if match == true {
					handlers.AcceptReq(cb, bot, mh)
					return
				}
			}
		}
	})
	log.Println("bot started")
	bot.Start()
}
