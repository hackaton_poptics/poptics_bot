# Makefile for releasing poptics_bot
#
# The release version is controlled from pkg/version

TAG=latest
NAME:=poptics_bot
DOCKER_REPOSITORY:=gitlab.com:4567/hackaton_poptics
DOCKER_IMAGE_NAME:=$(DOCKER_REPOSITORY)/$(NAME)
GIT_COMMIT:=$(shell git describe --dirty --always)
VERSION:=$(shell grep 'VERSION' pkg/version/version.go | awk '{ print $$4 }' | tr -d '"')
PORT:=1444


build:
	CGO_ENABLED=0 GOOS=linux go build -mod=vendor -ldflags -w cmd/bot/main.go

run:
	./main

vendor:
	go mod init
	go mod vendor

update-vendor:
	rm -r vendor go.mod go.sum
	make vendor


build-container:
	docker build -t $(DOCKER_IMAGE_NAME):$(VERSION) .

run-container:
	@docker run -dp $(PORT):$(PORT) --name=$(NAME) $(DOCKER_IMAGE_NAME):$(VERSION)

stop-container:
	@docker stop $(NAME)
	@docker ps

clear-container:
	@docker rm -f $(NAME)

push-container:
	docker tag $(DOCKER_IMAGE_NAME):$(VERSION) $(DOCKER_IMAGE_NAME):latest
	docker push $(DOCKER_IMAGE_NAME):$(VERSION)
	docker push $(DOCKER_IMAGE_NAME):latest

release:
	git tag $(VERSION)
	git push origin $(VERSION)