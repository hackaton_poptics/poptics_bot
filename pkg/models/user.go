package models

import "time"

type User struct {
	TelegramID int       `bson:"telegram_id"`
	Username   string    `bson:"username"`
	State      string    `bson:"state"`
	FirstName  string    `bson:"first_name"`
	LastName   string    `bson:"last_name"`
	LangCode   string    `bson:"lang_code"`
	LastMsgId  int       `bson:"last_msg_id"`
	CreatedOn  time.Time `bson:"created_on"`
}
