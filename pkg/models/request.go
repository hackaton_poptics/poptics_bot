package models

type Request struct {
	Sender         int    `json:"sender" bson:"sender"`
	SenderUsername string `json:"sender_username" bson:"sender_username"`
	Info           string `json:"info" bson:"info"`
	Type           string `json:"type"`
	Accepted       bool   `json:"accepted" bson:"accepted"`
}
