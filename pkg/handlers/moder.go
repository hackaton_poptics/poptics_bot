package handlers

import (
	tb "gopkg.in/tucnak/telebot.v2"
	"poptics_bot/pkg/models"
	"strconv"
)

func SignalAdmin(req *models.Request, bot *tb.Bot) (err error) {
	reqSender := strconv.Itoa(req.Sender)
	watchReqBtn := tb.InlineButton{
		Unique: "admin_" + req.Type + "_" + reqSender,
		Text:   "Watch request",
	}
	inlineKeys := [][]tb.InlineButton{
		[]tb.InlineButton{watchReqBtn},
		// ...
	}
	bot.Send(&tb.Chat{ID: -1001265100924}, "У Вас новая заявка на проверку.", &tb.ReplyMarkup{InlineKeyboard: inlineKeys})
	return nil
}
