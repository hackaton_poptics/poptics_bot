package handlers

import (
	tb "gopkg.in/tucnak/telebot.v2"
	"log"
	"poptics_bot/pkg/database"
	"poptics_bot/pkg/helpers"
)

// FSM implementation. Handles 'back_inline' callback_data
// TODO: store userStates in redis instead of mongodb
func BackInline(cb *tb.Callback, bot *tb.Bot, mh *database.MongoHandler) {
	user, err := GetUser(cb.Sender.ID, mh)
	if err != nil {
		log.Println(err)
	}

	switch user.State {
	case "requests_menu":
		newState := "home"
		err := helpers.UpdateUserState(cb.Sender.ID, newState, mh)
		if err != nil {
			log.Println(err)
		}
		StartInline(cb, bot, mh)
	case "request_menu":
		newState := "home"
		err := helpers.UpdateUserState(cb.Sender.ID, newState, mh)
		if err != nil {
			log.Println(err)
		}
		StartInline(cb, bot, mh)
	case "writing_description_participant":
		newState := "requests_menu"
		err := helpers.UpdateUserState(cb.Sender.ID, newState, mh)
		if err != nil {
			log.Println(err)
		}
		RequestMenu(cb, bot, mh)
	case "writing_description_lead":
		newState := "requests_menu"
		err := helpers.UpdateUserState(cb.Sender.ID, newState, mh)
		if err != nil {
			log.Println(err)
		}
		RequestMenu(cb, bot, mh)
	case "watching_list":
		newState := "list_menu"
		err := helpers.UpdateUserState(cb.Sender.ID, newState, mh)
		if err != nil {
			log.Println(err)
		}
		ListRequestsMenu(cb, bot, mh)
	default:
		log.Println("user come from strange place")
		newState := "home"
		err := helpers.UpdateUserState(cb.Sender.ID, newState, mh)
		if err != nil {
			log.Println(err)
		}
		StartInline(cb, bot, mh)
	}
}
