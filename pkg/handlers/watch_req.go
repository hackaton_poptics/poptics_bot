package handlers

import (
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	tb "gopkg.in/tucnak/telebot.v2"
	"log"
	"poptics_bot/pkg/database"
	"regexp"
	"strconv"
)

func WatchRequest(cb *tb.Callback, bot *tb.Bot, mh *database.MongoHandler) {
	re := regexp.MustCompile(`admin_(participant|lead)_(\d+)`)
	result := re.FindStringSubmatch(cb.Data)
	senderId, _ := strconv.Atoi(result[2])
	reqType := result[1]

	req, err := mh.GetReq(bson.M{"sender": senderId, "type": reqType})
	if err != nil {
		log.Println(err)
	}
	reqIdStr := strconv.Itoa(req.Sender)
	sender, err := mh.GetUser(bson.M{"telegram_id": req.Sender})
	if err != nil {
		log.Println(err)
	}

	acceptReqBtn := tb.InlineButton{
		Unique: "accept_req_" + req.Type + "_" + reqIdStr,
		Text:   "Accept",
	}
	declineReqBtn := tb.InlineButton{
		Unique: "decline_req_" + req.Type + "_" + reqIdStr,
		Text:   "Decline",
	}
	inlineKeys := [][]tb.InlineButton{
		[]tb.InlineButton{acceptReqBtn, declineReqBtn},
		// ...
	}

	text := fmt.Sprintf("from user: @%s\nrequest type:%s\nrequest info:\n%s", sender.Username, req.Type, req.Info)

	bot.Edit(cb.Message, text, &tb.ReplyMarkup{InlineKeyboard: inlineKeys})
	err = bot.Respond(cb, &tb.CallbackResponse{})
	if err != nil {
		log.Println(err)
	}
}

func AcceptReq(cb *tb.Callback, bot *tb.Bot, mh *database.MongoHandler) {
	re := regexp.MustCompile(`accept_req_(.*)_(\d+)$`)
	result := re.FindStringSubmatch(cb.Data)
	senderId, _ := strconv.Atoi(result[2])
	reqType := result[1]

	_, err := mh.UpdateReq(bson.M{"sender": senderId, "type": reqType}, bson.M{"$set": bson.M{"accepted": true}})
	if err != nil {
		log.Println(err)
	}
	bot.Edit(cb.Message, "Заявка принята и будет отображена в списке", tb.ParseMode("Markdown"))
	bot.Respond(cb, &tb.CallbackResponse{})
	bot.Send(&tb.User{ID: senderId}, "Ваша заявка принята. Желаем успешного поиска команды.")
}

func DeclineRequest(cb *tb.Callback, bot *tb.Bot, mh *database.MongoHandler) {
	re := regexp.MustCompile(`(\d+)$`)
	result := re.FindString(cb.Data)
	senderId, _ := strconv.Atoi(result)

	state := "decline_comment_" + result
	_, err := mh.UpdateUser(bson.M{"telegram_id": cb.Sender.ID},
		bson.M{"$set": bson.M{"state": state}})
	if err != nil {
		log.Println(err)
		return
	}
	err = mh.DeleteRequest(bson.M{"sender": senderId})
	if err != nil {
		log.Println(err)
	}
	bot.Edit(cb.Message, "Заявка отклонена. Отправитель оповещен.", tb.ParseMode("Markdown"))
	bot.Respond(cb, &tb.CallbackResponse{})
	bot.Send(&tb.User{ID: senderId}, "Ваша заявка отклонена, попробуйте снова.")
}
