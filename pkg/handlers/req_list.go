package handlers

import (
	"go.mongodb.org/mongo-driver/bson"
	tb "gopkg.in/tucnak/telebot.v2"
	"log"
	"poptics_bot/pkg/database"
	"poptics_bot/pkg/helpers"
	"regexp"
	"strconv"
	"strings"
)

func ListRequestsMenu(cb *tb.Callback, bot *tb.Bot, mh *database.MongoHandler) {
	listParticipantsBtn := tb.InlineButton{
		Unique: "list_requests_participant",
		Text:   "Заявки участников",
	}
	listLeadsBtn := tb.InlineButton{
		Unique: "list_requests_leads",
		Text:   "Заявки капитанов",
	}
	backBtn := tb.InlineButton{
		Unique: "back_inline",
		Text:   "«",
	}
	inlineKeys := [][]tb.InlineButton{
		[]tb.InlineButton{listParticipantsBtn},
		[]tb.InlineButton{listLeadsBtn},
		[]tb.InlineButton{backBtn},
		// ...
	}
	err := helpers.UpdateUserState(cb.Sender.ID, "requests_menu", mh)
	if err != nil {
		log.Println(err)
	}
	text := "Какой тип заявок Вам интересен?"
	bot.Edit(cb.Message, text, tb.ParseMode("Markdown"), &tb.ReplyMarkup{InlineKeyboard: inlineKeys})
	bot.Respond(cb, &tb.CallbackResponse{})
}

func GetRequestsFromParticipants(cb *tb.Callback, bot *tb.Bot, mh *database.MongoHandler, fromOtherPage bool) {
	reqs, err := mh.GetRequests(bson.M{"type": "participant", "accepted": true})
	if err != nil {
		log.Println(err)
	}
	// pagination stuff
	var inlineKeys = [][]tb.InlineButton{}
	err = helpers.UpdateUserState(cb.Sender.ID, "watching_list", mh)
	if err != nil {
		log.Println(err)
	}
	// user come from '>>' button
	if fromOtherPage {
		if strings.Contains(cb.Data, "participant_prev_page") {
			re := regexp.MustCompile(`(.*)_prev_page_(\d+)$`)
			res := re.FindStringSubmatch(cb.Data)
			reqType := res[1]
			reqType = reqType[1:]
			prevPage, _ := strconv.Atoi(res[2])
			prevPage = prevPage - 1
			inlineKeys, err = helpers.GenPaginationKeyboard(reqType, reqs, prevPage, prevPage+1)
			if err != nil {
				log.Println(err)
			}
		} else {
			re := regexp.MustCompile(`(.*)_next_page_(\d+)$`)
			res := re.FindStringSubmatch(cb.Data)
			reqType := res[1]
			reqType = reqType[1:]
			nextPage, _ := strconv.Atoi(res[2])
			inlineKeys, err = helpers.GenPaginationKeyboard(reqType, reqs, nextPage-1, nextPage)
			if err != nil {
				log.Println(err)
			}
		}
		// user come from menu
	} else {
		if len(reqs) >= 10 {
			inlineKeys, err = helpers.GenPaginationKeyboard("participant", reqs)
			if err != nil {
				log.Println(err)
			}
		} else {
			var kb [][]tb.InlineButton
			for _, req := range reqs {
				log.Println(req)
				var btn = []tb.InlineButton{}
				btn = append(btn, tb.InlineButton{
					Unique: "detail_req_participant_" + req.SenderUsername,
					Text:   req.Info,
				})
				kb = append(kb, btn)
			}
			var btnBack = []tb.InlineButton{}
			btnBack = append(btnBack, tb.InlineButton{
				Unique: "back_inline",
				Text:   "В меню",
			})
			for _, btn := range kb {
				inlineKeys = append(inlineKeys, btn)
			}
			inlineKeys = append(inlineKeys, btnBack)
		}
	}
	text := "Ознакомьтесь со список участников:"
	bot.Edit(cb.Message, text, tb.ParseMode("Markdown"), &tb.ReplyMarkup{InlineKeyboard: inlineKeys})
	bot.Respond(cb, &tb.CallbackResponse{})
}

func DetailRequestsFromParticipant(cb *tb.Callback, bot *tb.Bot, mh *database.MongoHandler) {
	re := regexp.MustCompile(`detail_req_participant_(.*)$`)
	result := re.FindString(cb.Data)
	result = result[23:]
	req, err := mh.GetReq(bson.M{"type": "participant", "sender_username": result, "accepted": true})
	if err != nil {
		log.Println(err)
		return
	}
	bot.Respond(cb, &tb.CallbackResponse{Text: req.Info, ShowAlert: true})

}

func GetRequestsFromLeads(cb *tb.Callback, bot *tb.Bot, mh *database.MongoHandler, fromOtherPage bool) {
	reqs, err := mh.GetRequests(bson.M{"type": "lead", "accepted": true})
	if err != nil {
		log.Println(err)
	}

	// pagination stuff
	var inlineKeys = [][]tb.InlineButton{}
	err = helpers.UpdateUserState(cb.Sender.ID, "watching_list", mh)
	if err != nil {
		log.Println(err)
	}

	// user come from '>>' button
	if fromOtherPage {
		if strings.Contains(cb.Data, "lead_prev_page") {
			re := regexp.MustCompile(`(.*)_prev_page_(\d+)$`)
			res := re.FindStringSubmatch(cb.Data)
			reqType := res[1]
			reqType = reqType[1:]
			prevPage, _ := strconv.Atoi(res[2])
			prevPage = prevPage - 1
			inlineKeys, err = helpers.GenPaginationKeyboard(reqType, reqs, prevPage, prevPage+1)
			if err != nil {
				log.Println(err)
			}
		} else {
			re := regexp.MustCompile(`(.*)_next_page_(\d+)$`)
			res := re.FindStringSubmatch(cb.Data)
			reqType := res[1]
			reqType = reqType[1:]
			nextPage, _ := strconv.Atoi(res[2])
			inlineKeys, err = helpers.GenPaginationKeyboard(reqType, reqs, nextPage-1, nextPage)
			if err != nil {
				log.Println(err)
			}
		}
		// user come from menu
	} else {
		if len(reqs) >= 10 {
			inlineKeys, err = helpers.GenPaginationKeyboard("lead", reqs)
			if err != nil {
				log.Println(err)
			}
		} else {
			var kb [][]tb.InlineButton
			for _, req := range reqs {
				log.Println(req)
				var btn = []tb.InlineButton{}
				btn = append(btn, tb.InlineButton{
					Unique: "detail_req_lead_" + req.SenderUsername,
					Text:   req.Info,
				})

				kb = append(kb, btn)
			}
			var btnBack = []tb.InlineButton{}
			btnBack = append(btnBack, tb.InlineButton{
				Unique: "back_inline",
				Text:   "В меню",
			})
			for _, btn := range kb {
				inlineKeys = append(inlineKeys, btn)
			}
			inlineKeys = append(inlineKeys, btnBack)
		}
	}
	text := "Ознакомьтесь со список участников:"
	bot.Edit(cb.Message, text, tb.ParseMode("Markdown"), &tb.ReplyMarkup{InlineKeyboard: inlineKeys})
	bot.Respond(cb, &tb.CallbackResponse{})
}

func DetailRequestsFromLead(cb *tb.Callback, bot *tb.Bot, mh *database.MongoHandler) {
	re := regexp.MustCompile(`detail_req_lead_(.*)$`)
	result := re.FindStringSubmatch(cb.Data)
	senderUname := result[1]
	req, err := mh.GetReq(bson.M{"type": "lead", "sender_username": senderUname, "accepted": true})
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(req)
	bot.Respond(cb, &tb.CallbackResponse{Text: req.Info, ShowAlert: true})
}
