package handlers

import (
	"go.mongodb.org/mongo-driver/bson"
	tb "gopkg.in/tucnak/telebot.v2"
	"log"
	"poptics_bot/pkg/database"
	"poptics_bot/pkg/helpers"
	"poptics_bot/pkg/models"
)

func RequestMenu(cb *tb.Callback, bot *tb.Bot, mh *database.MongoHandler) {
	lfTeamBtn := tb.InlineButton{
		Unique: "lf_team",
		Text:   "Ищу команду",
	}
	lfParticipantBtn := tb.InlineButton{
		Unique: "lf_participant",
		Text:   "Ищу людей",
	}
	backBtn := tb.InlineButton{
		Unique: "back_inline",
		Text:   "«",
	}
	inlineKeys := [][]tb.InlineButton{
		[]tb.InlineButton{lfTeamBtn},
		[]tb.InlineButton{lfParticipantBtn},
		[]tb.InlineButton{backBtn},
		// ...
	}
	err := helpers.UpdateUserState(cb.Sender.ID, "request_menu", mh)
	if err != nil {
		log.Println(err)
	}
	text := `*Подать заявку*

Сделайте свой выбор:`
	bot.Edit(cb.Message, text, tb.ParseMode("Markdown"), &tb.ReplyMarkup{InlineKeyboard: inlineKeys})
	bot.Respond(cb, &tb.CallbackResponse{})
}

// This function handles incoming requests from IT-people
// that looking for team
func RegisterParticipant(cb *tb.Callback, bot *tb.Bot, mh *database.MongoHandler) {
	var text string
	user, err := GetUser(cb.Sender.ID, mh)
	if err != nil {
		log.Println(err)
		return
	}
	// Update user state
	_, err = mh.UpdateUser(bson.M{"telegram_id": user.TelegramID},
		bson.M{"$set": bson.M{"state": "writing_description_participant"}})
	if err != nil {
		log.Println(err)
		return
	}
	// Get request from database
	req, err := mh.GetReq(bson.M{"type": "participant", "sender": user.TelegramID})
	if err != nil {
		log.Println(err)
	}
	if req.Sender != 0 {
		text = `Вы уже подавали заявку.`
		bot.Respond(cb, &tb.CallbackResponse{Text: text, ShowAlert: true})
		return
	} else {
		text = `
Для регистрации заявки, расскажите о себе.

Пример:
Ищу тиму.
Мой стек: Rust, Docker, mongodb
github: https://github.com/myprofilelink
Связь: @username`
	}
	inlineKeys := [][]tb.InlineButton{
		[]tb.InlineButton{tb.InlineButton{
			Unique: "back_inline",
			Text:   "«",
		}},
		// ...
	}
	bot.Edit(cb.Message, text, tb.ParseMode("Markdown"), &tb.ReplyMarkup{InlineKeyboard: inlineKeys})
	bot.Respond(cb, &tb.CallbackResponse{})
}

func SaveParticipantReq(msg *tb.Message, bot *tb.Bot, mh *database.MongoHandler) {
	req := models.Request{
		Sender:         msg.Sender.ID,
		SenderUsername: msg.Sender.Username,
		Info:           msg.Text,
		Type:           "participant",
		Accepted:       false,
	}
	// create new document in requests collection
	_, err := mh.AddParticipantReq(&req)
	if err != nil {
		log.Println(err)
		return
	}

	err = SignalAdmin(&req, bot)
	if err != nil {
		log.Println("failed to send req to admin: ", err)
	}

	inlineKeys := [][]tb.InlineButton{
		[]tb.InlineButton{tb.InlineButton{
			Unique: "home",
			Text:   "🛰",
		}},
		// ...
	}
	bot.Send(msg.Sender, "Ваша заявка принята в работу. Вы получите оповещение.", &tb.ReplyMarkup{InlineKeyboard: inlineKeys})
}

// This function handles incoming requests from leads
// that looking for participants
func RegisterLead(cb *tb.Callback, bot *tb.Bot, mh *database.MongoHandler) {
	var text string
	user, err := GetUser(cb.Sender.ID, mh)
	if err != nil {
		log.Println(err)
		return
	}
	_, err = mh.UpdateUser(bson.M{"telegram_id": user.TelegramID},
		bson.M{"$set": bson.M{"state": "writing_description_lead"}})
	if err != nil {
		log.Println(err)
		return
	}
	req, err := mh.GetReq(bson.M{"type": "lead", "sender": user.TelegramID})
	if err != nil {
		log.Println(err)
	}
	if req.Sender != 0 {
		text = `Вы уже подавали заявку.`
		bot.Respond(cb, &tb.CallbackResponse{Text: text, ShowAlert: true})
		return
	} else {
		text = `
To register your request, please tell some information about your team.

Example:
Ищу людей в тиму.
Нужны: Rust, Docker, mongodb
Связь: @username`
	}
	inlineKeys := [][]tb.InlineButton{
		[]tb.InlineButton{tb.InlineButton{
			Unique: "back_inline",
			Text:   "«",
		}},
		// ...
	}
	bot.Edit(cb.Message, text, tb.ParseMode("Markdown"), &tb.ReplyMarkup{InlineKeyboard: inlineKeys})
	bot.Respond(cb, &tb.CallbackResponse{})
}

func SaveLeadReq(msg *tb.Message, bot *tb.Bot, mh *database.MongoHandler) {
	req := models.Request{
		Sender:         msg.Sender.ID,
		SenderUsername: msg.Sender.Username,
		Info:           msg.Text,
		Type:           "lead",
		Accepted:       false,
	}
	// save request in requests collection
	_, err := mh.AddLeadReq(&req)
	if err != nil {
		log.Println(err)
		return
	}
	// notify admin in channel about new request to approve
	err = SignalAdmin(&req, bot)
	if err != nil {
		log.Println("failed to send req to admin: ", err)
	}
	inlineKeys := [][]tb.InlineButton{
		[]tb.InlineButton{tb.InlineButton{
			Unique: "home",
			Text:   "🛰",
		}},
		// ...
	}
	bot.Send(msg.Sender, "Ваша заявка принята в работу. Вы получите оповещение.", &tb.ReplyMarkup{InlineKeyboard: inlineKeys})
}
