package handlers

import (
	"go.mongodb.org/mongo-driver/bson"
	tb "gopkg.in/tucnak/telebot.v2"
	"log"
	"poptics_bot/pkg/database"
	"poptics_bot/pkg/models"
	"time"
)

func InitUser(msg *tb.Message, mh *database.MongoHandler) (user models.User, err error) {
	user = models.User{
		TelegramID: msg.Sender.ID,
		Username:   msg.Sender.Username,
		State:      "start",
		FirstName:  msg.Sender.FirstName,
		LastName:   msg.Sender.LastName,
		LangCode:   msg.Sender.LanguageCode,
		LastMsgId:  msg.ID,
		CreatedOn:  time.Now(),
	}
	_, err = mh.GetUser(bson.M{"telegram_id": msg.Sender.ID})
	if err != nil {
		_, err = mh.AddUser(&user)
		if err != nil {
			log.Println(err)
			return
		}
	}
	return user, nil
}

func Start(msg *tb.Message, bot *tb.Bot, mh *database.MongoHandler) {
	_, err := InitUser(msg, mh)
	if err != nil {
		log.Println(err)
		return
	}
	applyRequestBtn := tb.InlineButton{
		Unique: "apply_request",
		Text:   "Подать заявку",
	}
	listRequstsBtn := tb.InlineButton{
		Unique: "list_requests",
		Text:   "Список заявок",
	}
	inlineKeys := [][]tb.InlineButton{
		//[]tb.InlineButton{lfTeamBtn},
		[]tb.InlineButton{applyRequestBtn},
		[]tb.InlineButton{listRequstsBtn},
		// ...
	}
	text := `*∏ρØƒuñçτØρ Øπτµç∑* | 👁‍🗨›››› hackaton

Сделайте свой выбор:`
	bot.Send(msg.Sender, text, tb.ParseMode("Markdown"), &tb.ReplyMarkup{InlineKeyboard: inlineKeys})
}

func StartInline(cb *tb.Callback, bot *tb.Bot, mh *database.MongoHandler) {
	applyRequestBtn := tb.InlineButton{
		Unique: "apply_request",
		Text:   "Подать заявку",
	}
	listRequstsBtn := tb.InlineButton{
		Unique: "list_requests",
		Text:   "Список заявок",
	}
	inlineKeys := [][]tb.InlineButton{
		//[]tb.InlineButton{lfTeamBtn},
		[]tb.InlineButton{applyRequestBtn},
		[]tb.InlineButton{listRequstsBtn},
		// ...
	}
	text := `*∏ρØƒuñçτØρ Øπτµç∑* | 👁‍🗨›››› hackaton

Сделайте свой выбор:`
	bot.Edit(cb.Message, text, tb.ParseMode("Markdown"), &tb.ReplyMarkup{InlineKeyboard: inlineKeys})
	bot.Respond(cb, &tb.CallbackResponse{})
}
