package handlers

import (
	"go.mongodb.org/mongo-driver/bson"
	"log"
	"poptics_bot/pkg/database"
	"poptics_bot/pkg/models"
)

func GetUser(tID int, mh *database.MongoHandler) (user models.User, err error) {
	user, err = mh.GetUser(bson.M{"telegram_id": tID})
	if err != nil {
		log.Println(err)
		return
	}
	return
}
