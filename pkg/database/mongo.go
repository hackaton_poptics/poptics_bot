package database

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"poptics_bot/pkg/models"
	"time"
)

const DefaultDatabase = "telegram-bot"

type MongoHandler struct {
	client   *mongo.Client
	database string
}

//MongoHandler Constructor
func NewHandler(address string) *MongoHandler {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	cl, _ := mongo.Connect(ctx, options.Client().ApplyURI(address))
	mh := &MongoHandler{
		client:   cl,
		database: DefaultDatabase,
	}
	return mh
}

func (mh *MongoHandler) AddUser(c *models.User) (res *mongo.InsertOneResult, err error) {
	log.Println("call adduser")
	collection := mh.client.Database(mh.database).Collection("users")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result, err := collection.InsertOne(ctx, c)
	return result, err
}

func (mh *MongoHandler) AddParticipantReq(p *models.Request) (res *mongo.InsertOneResult, err error) {
	collection := mh.client.Database(mh.database).Collection("requests")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result, err := collection.InsertOne(ctx, p)
	return result, err
}

func (mh *MongoHandler) AddLeadReq(p *models.Request) (res *mongo.InsertOneResult, err error) {
	collection := mh.client.Database(mh.database).Collection("requests")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result, err := collection.InsertOne(ctx, p)
	return result, err
}

func (mh *MongoHandler) UpdateUser(filter interface{}, update interface{}) (mongo.UpdateResult, error) {
	collection := mh.client.Database(mh.database).Collection("users")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return mongo.UpdateResult{}, err
	}
	return mongo.UpdateResult{}, nil
}

func (mh *MongoHandler) UpdateReq(filter interface{}, update interface{}) (mongo.UpdateResult, error) {
	collection := mh.client.Database(mh.database).Collection("requests")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return mongo.UpdateResult{}, err
	}
	return mongo.UpdateResult{}, nil
}

func (mh *MongoHandler) GetUser(filter interface{}) (user models.User, err error) {
	collection := mh.client.Database("telegram-bot").Collection("users")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	err = collection.FindOne(ctx, filter).Decode(&user)
	if err != nil {
		log.Println(" get user error ", err)
		return user, err
	}
	return user, nil
}

func (mh *MongoHandler) GetReq(filter interface{}) (req models.Request, err error) {
	collection := mh.client.Database("telegram-bot").Collection("requests")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	err = collection.FindOne(ctx, filter).Decode(&req)
	if err != nil {
		log.Print(err)
		return req, err
	}
	return req, nil
}

func (mh *MongoHandler) DeleteRequest(filter interface{}) (err error) {
	collection := mh.client.Database(mh.database).Collection("requests")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	res, err := collection.DeleteOne(ctx, filter)
	if err != nil {
		log.Print(err)
	}
	log.Println(res.DeletedCount)
	return nil
}

func (mh *MongoHandler) GetRequests(filter interface{}) (requests []models.Request, err error) {
	collection := mh.client.Database(mh.database).Collection("requests")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	cursor, err := collection.Find(ctx, filter)
	for cursor.Next(context.TODO()) {
		var request models.Request
		err := cursor.Decode(&request)
		if err != nil {
			log.Println(err)
		}
		requests = append(requests, request)

	}
	return requests, nil
}