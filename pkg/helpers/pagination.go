package helpers

import (
	"fmt"
	tb "gopkg.in/tucnak/telebot.v2"
	"log"
	"poptics_bot/pkg/models"
	"strconv"
)

func GenPaginationKeyboard(reqType string, reqs []models.Request, pageIndexes ...int) (inlineKeyboard [][]tb.InlineButton, err error) {
	pages := len(reqs) / 10
	var currentPage = 1
	var prevPage int
	var nextPage int
	log.Println(nextPage)

	var kb [][]tb.InlineButton
	var navigationRow = []tb.InlineButton{}
	if pageIndexes != nil {
		currentPage = pageIndexes[0] + 1
		prevPage = currentPage - 1
		var prevPageBtnText string
		if prevPage == 0 {
			prevPage = currentPage
			prevPageBtnText = " "
		} else {
			prevPageBtnText = fmt.Sprintf("%d/%d <<", pageIndexes[1]-1, pages)
		}
		nextPage = pageIndexes[1] + 1
		// check if page will be last
		var nextPageBtnText string
		if nextPage > pages {
			nextPage = currentPage
			nextPageBtnText = " "
		} else {
			nextPageBtnText = fmt.Sprintf("%d/%d >>", pageIndexes[1]+1, pages)
		}
		offset := (currentPage-1)*10 + 1

		for _, req := range reqs[offset : offset+10] {
			log.Println(req)
			var btn = []tb.InlineButton{}
			btnCallbackData := fmt.Sprintf("detail_req_%s_%s", reqType, req.SenderUsername)
			log.Println(btnCallbackData)
			btn = append(btn, tb.InlineButton{
				Unique: btnCallbackData,
				Text:   req.Info,
			})
			kb = append(kb, btn)
		}
		navigationRow = append(navigationRow, tb.InlineButton{
			Unique: reqType + "_prev_page_" + strconv.Itoa(prevPage),
			Text:   prevPageBtnText,
		})
		navigationRow = append(navigationRow, tb.InlineButton{
			Unique: "back_inline",
			Text:   "В меню",
		})
		navigationRow = append(navigationRow, tb.InlineButton{
			Unique: reqType + "_next_page_" + strconv.Itoa(nextPage),
			Text:   nextPageBtnText,
		})
	} else {
		// Init page
		currentPage = 1
		prevPage = pages
		nextPage = 2
		var nextPageBtnText string
		if nextPage > pages {
			nextPage = currentPage
			nextPageBtnText = " "
		} else {
			nextPageBtnText = fmt.Sprintf("%d/%d >>", nextPage, pages)
		}
		for _, req := range reqs[0:10] {
			log.Println(req)
			var btn = []tb.InlineButton{}
			btnCallbackData := fmt.Sprintf("detail_req_%s_%s", reqType, req.SenderUsername)
			log.Println(btnCallbackData)
			btn = append(btn, tb.InlineButton{
				Unique: "detail_req_" + reqType + "_" + req.SenderUsername,
				Text:   req.Info,
			})
			kb = append(kb, btn)
		}

		navigationRow = append(navigationRow, tb.InlineButton{
			Unique: reqType + "_prev_page_" + strconv.Itoa(prevPage),
			Text:   " ",
		})
		navigationRow = append(navigationRow, tb.InlineButton{
			Unique: "back_inline",
			Text:   "В меню",
		})
		navigationRow = append(navigationRow, tb.InlineButton{
			Unique: reqType + "_next_page_" + strconv.Itoa(nextPage),
			Text:   nextPageBtnText,
			//Text:   strconv.Itoa(nextPage) + "/" + strconv.Itoa(pages) + " >>",
		})
	}

	log.Println(fmt.Sprintf("\ncurrent page: %d\nnext_page: %d\nprev_page: %d", currentPage, nextPage, prevPage))

	kb = append(kb, navigationRow)
	return kb, nil
}
