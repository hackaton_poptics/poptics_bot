package helpers

import (
	tb "gopkg.in/tucnak/telebot.v2"
	"log"
	"os"
	"time"
)

func GetBot(pollerType string) (bot *tb.Bot, err error) {
	if pollerType == "webhook" {
		appPort := os.Getenv("APP_PORT")
		publicURL := os.Getenv("PUBLIC_URL")
		certPath := os.Getenv("CERT_PATH")
		token := os.Getenv("BOT_TOKEN")
		webhook := &tb.Webhook{
			Listen: ":" + appPort,
			Endpoint: &tb.WebhookEndpoint{
				PublicURL: publicURL,
				Cert:      certPath,
			},
		}
		bot, err := tb.NewBot(tb.Settings{
			Token:  token,
			Poller: webhook,
		})
		if err != nil {
			log.Fatal(err)
		}
		return bot, err
	} else {
		token := os.Getenv("BOT_TOKEN")
		bot, err := tb.NewBot(tb.Settings{
			Token:  token,
			Poller: &tb.LongPoller{Timeout: 10 * time.Second},
		})
		return bot, err
	}
}
