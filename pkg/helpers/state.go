package helpers

import (
	"go.mongodb.org/mongo-driver/bson"
	"log"
	"poptics_bot/pkg/database"
)

func UpdateUserState(senderId int, newState string, mh *database.MongoHandler) (err error) {
	_, err = mh.UpdateUser(bson.M{"telegram_id": senderId}, bson.M{"$set": bson.M{"state": newState}})
	if err != nil {
		log.Println(err)
		return
	}
	return nil
}
